package com.tw;

public class Book {

	public String title;
	public String author;
	public int publishedYear;
	public String isbn;

	public Book(String title, String author, int publishedYear, String isbn) {
		this.title = title;
		this.author = author;
		this.publishedYear = publishedYear;
		this.isbn = isbn;
	}

	public String getBookInfo() {
		return "Title: " + title + ", Author: " + author + ", Year: " + publishedYear + ", ISBN: " + isbn;
	}

}
