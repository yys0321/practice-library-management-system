package com.tw;

import java.awt.print.Book;
import java.util.ArrayList;

public class Library {

	private final ArrayList<Book> books;

	public Library() {
		books = new ArrayList<>();
	}

	public void addBook(Book book) {
		books.add(book);
	}

	public void removeBook(Book book) {
		books.remove(book);
	}

	public ArrayList<Book> getBookPublishedAfterYear(int year) {
		ArrayList<Book> result = new ArrayList<>();

		for (Book book : books) {
			if (book.publishedYear > year) {
				result.add(book);
			}
		}

		return result;
	}

	public ArrayList<String> getAuthorsOfBooksPublishedBeforeYear(int year) {
		ArrayList<String> result = new ArrayList<>();

		for (Book book : books) {
			if (book.publishedYear < year) {
				result.add(book.author);
			}
		}

		return result;
	}
}
